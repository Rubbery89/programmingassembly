.MODEL FLAT, C
.DATA
   discount real8 12.0
   cantProduct real8 20.0
   restrictDiscount DD 10
   totalDiscount real8 0.0
.CODE
CalculateSaleWithDiscountAsm PROC

		push ebp
        mov ebp,esp
        push ebx
        push esi
        push edi

        mov edi,[ebp+8] 
        mov esi,[ebp+12] 
        mov ecx,[ebp+16] 
		mov edx,[ebp+20] 

        cmp ecx,0
        jle withOutDiscount

		cmp edx,0 
		jle withOutDiscount

		xor eax, eax
		cmp edx, restrictDiscount
		jge hasDiscount		
		jmp calculateSaleWithDiscount

hasDiscount:
        fld discount
        fadd discount
		add restrictDiscount, 10
		cmp edx, restrictDiscount
		jge hasDiscount
        

calculateSale:
        shl ecx,3 
		xor ebx,ebx 
        fld real8 ptr [esi+ebx]

	    fmul [cantProduct]	
		fsub st(0), st(1)
		fst real8 ptr [edi]
		cmp ebx,ecx
		jmp withOutDiscount
 
calculateSaleWithDiscount:
        shl ecx,3 
		xor ebx,ebx 	

        fld real8 ptr [esi+ebx]
	    fmul [cantProduct]			
		fst real8 ptr [edi] 			
		cmp ebx,ecx
			

withOutDiscount:
    pop edi
    pop esi
    pop ebx
    pop ebp
    ret

CalculateSaleWithDiscountAsm ENDP
END