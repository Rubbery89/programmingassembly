// Normal.Almacen.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include "pch.h"
#include <iostream>
using namespace std;

extern "C" void CalculateSaleWithDiscountAsm(double* Area, double* priceByProduct, int tam, int cantProduct);

int main()
{
	int cantProduct = 0;
	cout << "Type the cant of products that you wish purchase: "; cin >> cantProduct;
	cout << "By each 10 products you get $" << 12 << " dollars of discount over your purchase \n" << endl;
		
	double priceProduct[] = { 20.0 };
	double result[1];
	const int lengthPrices = sizeof(priceProduct) / sizeof(double);	
	CalculateSaleWithDiscountAsm(result, priceProduct, lengthPrices, cantProduct);
	cout << "The price of product is of $: " << priceProduct[lengthPrices-1] << " dolares \ttotal sale with discount: " << result[lengthPrices-1] << endl;
	
	system("PAUSE");

	return 0;
}

