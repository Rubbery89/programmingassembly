
#include "pch.h"
#include <iostream>

using namespace std;

extern "C" void AreaPF(double radio, double* Area);
extern "C" void AreaPF2(double* areas, double* radio, int cantidad);

void Mostrar(int* valCuadra, const int* valEntra, int numByte, int suma) {

	for (int cont = 0; cont < numByte; cont++) {
		std::cout << "Contador: " << cont << " \tAreglo: " << valEntra[cont] << " \tCuadrado: " << valCuadra[cont] << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Suma " << suma << std::endl;
}

void Mostrar2(double* valCuadra, const double* valEntra, int numByte) {

	for (int cont = 0; cont < numByte; cont++) {
		std::cout << "Contador: " << cont << " \tAreglo: " << valEntra[cont] << " \tArea: " << valCuadra[cont] << std::endl;
	}
}

int main()
{
	int valEntra[] = { 2, 3, 5, 8, 13, 21, 34 };
	double radio[] = { 1.111, 2.222, 3.3333, 4.4444 };
	const int numRad = sizeof(radio) / sizeof(double);
	const int numByte = sizeof(valEntra) / sizeof(int);
	int valCuadra[numByte];
	double valArea[numRad];

	AreaPF2(valArea, radio, numRad);

	cout << &valArea << endl;
	/*for (char con = 0; con < numRad; con++)
	{
		double Area;
		AreaPF(radio[con], &Area);
		cout << "Radio " << radio[con] << "  \tArea: " << Area << endl;
	}*/

	system("PAUSE");
	return 0;
}

