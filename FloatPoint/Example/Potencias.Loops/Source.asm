.MODEL FLAT, C
.DATA
  dosDouble real8 2.0
.CODE

downLevelProcess PROC
	    push ebp
        mov ebp,esp
        push ebx
        push esi
        push edi

        mov edi,[ebp+8] 
        mov esi,[ebp+12] 
        mov ecx,[ebp+16] 

        cmp ecx,0
        jle AregloVacio
        shl ecx,3   
        xor ebx,ebx  

 @@:    fld real8 ptr [esi+ebx]
        ;fmul st(0),st(0)
        fmul [dosDouble]
        ;fmulp


        fst real8 ptr [edi] 
		add ebx,8
		add edi,8
		cmp ebx,ecx
        jl @B 
		AregloVacio:
        pop edi
        pop esi
        pop ebx
        pop ebp
        ret
downLevelProcess ENDP
END