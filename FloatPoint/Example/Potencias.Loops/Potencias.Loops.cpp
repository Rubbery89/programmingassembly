#include "pch.h"
#include <iostream>
using namespace std;
extern "C" void downLevelProcess(double* result, double* valEntra, int number);

void Show(const double* valEntra, int numByte, int number) {
	int i = 1;
	for (int cont = 0; cont < numByte; cont++) {
		
		cout << "Potency: " << 2.222 << "^" << i << " \tEqual: " << valEntra[cont]  << endl;
		i++;
	}
}

int main()
{
	int n = 0, x = 0, i = 1, auxAsm = 0;
	std::cout << "Which number do you want to get it potency: "; std::cin >> x;
	std::cout << "How many potencies do you want to calculate: "; std::cin >> n;
	auxAsm = x;
	if (n > 0 && x > 0)
	{
		int aux = x;
		do 
		{
			x *= aux;
			
			//std::cout << x << endl;
			i++;
			cout << "Potency: " << aux << "^" << i << " \tEqual: " << x << endl;
		} while (n != i);

		
	}		
	else std::cout << "======> you must select a number greather than a 0\n\n" << std::endl;

	std::cout << "\n\n_____________________________________________________________" << std::endl;
	std::cout << "_______________________ASSEMBLY______________________________" << std::endl;

	double potencia[] = { 1.111, 2.222, 4.4444 ,8.88888, 17.7778, 35.5556, 71.1112 };
	const int z = sizeof(potencia) / sizeof(double);

	double result[8];
	downLevelProcess(result, potencia, z);
	Show(result,z, auxAsm);
	system("PAUSE");
	return 0;
}
