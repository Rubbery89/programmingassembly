.386
.model flat, c
.data
  ;vbles problem color balls to apply discount
  totalPurchase real8 435.0
  ballWhite DD 0
  ballGreen DD 1
  ballYellow DD 2
  ballBlue DD 3
  ballRed DD 4
  DisWhite real8 0.0
  DisGreen real8 0.1
  DisYellow real8 0.25
  DisBlue real8 0.5
  DisRed real8 1.0

  initValue DD 1

.code
;1 Conditionals
ProcessDownLevel PROC
  push ebp
  mov ebp, esp
  mov eax, [ebp+8] ; color param
  fld [totalPurchase] ; total sale

  cmp eax, ballWhite
  jz white

  cmp eax, ballGreen
  je green

  cmp eax, ballYellow
  je yellow

  cmp eax, ballBlue
  je blue

  cmp eax, ballRed
  je red

  white:
	fld [DisWhite]
 	fmul st(0),st(1) 	
	jmp finallyProcess
  
  green:
	fld [DisGreen]
 	fmul st(0),st(1) 
	jmp finallyProcess

  yellow: 
	fld [DisYellow]
 	fmul st(0),st(1) 
	jmp finallyProcess

  blue:
	fld [DisBlue]
 	fmul st(0),st(1) 
	jmp finallyProcess

  red:
	fld [DisRed]
 	fmul st(0),st(1) 
	jmp finallyProcess

	finallyProcess:
	 mov ebx,[ebp+12]
     fst real8 ptr [ebx] 
	 pop ebp
	 ret
ProcessDownLevel ENDP
END