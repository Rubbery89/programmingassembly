#include "pch.h"
#include <iostream>
#include <ctime>

using namespace std;
extern "C" void ProcessDownLevel(int color, double* totalPurchase);

int main()
{
	double discount_white = 0, discount_green = 0.1, discount_yellow = 0.25, discount_blue = 0.5, discount_red = 1;
	srand(time(NULL));
	int color = rand() % 5;
	double discount = 0;
	
	//Init value purchase
	double totalDiscount, saleWithDiscount = 435;
	ProcessDownLevel(color, &totalDiscount);

	if (color == 0)		
		std::cout << "You have selected white color " << discount_white*100 << " % discount\n" << endl;
	else if (color == 1)
		std::cout << "You have selected green color " << discount_green*100 << " % discount\n" << endl;
	else if (color == 2)
		std::cout << "You have selected yellow color " <<discount_yellow*100 << " % discount\n" << endl;
	else if (color == 3)
		std::cout << "You have selected blue color " << discount_blue*100 << " % discount\n" << endl;
	else if (color == 4)
		std::cout << "You have selected red color " << discount_red*100 << " % discount\n" << endl;
	
	std::cout << "total sale: $" << saleWithDiscount << " dollars" << endl;
	std::cout << "total discount: $" << totalDiscount << " dollars \nTotal to pay: $" << saleWithDiscount - totalDiscount << " dollars\n\n" << endl;
	system("PAUSE");

	return 0;
}
