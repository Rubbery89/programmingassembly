#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

//extern "C" int functionC(int precio, int tipo, int tamanio);
extern "C" void AreaPF(double radio, double* Area);
extern "C" void AreaPF2(double* areas, double* radio, int cantidad);

void _AreaPF();
void _AreaPF2();
void Mostrar2(double* valCuadra, const double* valEntra, int numByte);

void _AreaPF() {
	int valEntra[] = { 2, 3, 5, 8, 13, 21, 34 };
	double radio[] = { 1.111, 2.222, 3.3333, 4.4444 };
	const int numRad = sizeof(radio) / sizeof(double);
	const int numByte = sizeof(valEntra) / sizeof(int);
	int valCuadra[numByte];
	double valArea[numRad];


	for (char con = 0; con < numRad; con++)
	{
		double Area;
		AreaPF(radio[con], &Area);
		cout << "Radio " << radio[con] << "  \tArea: " << Area << endl;
	}
	system("PAUSE");
}

void _AreaPF2() {
	double areas[] = { 2.2, 3.3, 5.5, 8.6 };
	double radio[] = { 1.111, 2.222, 3.3333, 4.4444 };
	const int numRad = sizeof(radio) / sizeof(double);
	const int numByte = sizeof(areas) / sizeof(int);
	int valCuadra[numByte];
	double valArea[numRad];

	AreaPF2(areas, radio, numRad);
	Mostrar2(areas, radio, numRad);
	system("PAUSE");
}

int main()
{
	_AreaPF();
	//_AreaPF2();
	/*
	int tamanio;
	int tipo; // A = 3; B = 4;
	int precio = 120;

	cout << "Ingrese el tipo de la uva:  A = 3; B = 4 : ";
	cin >> tipo;

	cout << "Ingrese el tamaño de la uva 1 (o) 2 : ";
	cin >> tamanio;

	int value = functionC(precio, tipo, tamanio);

	if (tipo == 3)
	{
		if (tamanio == 1)
		{
			cout << "Es tipo A tamaño 1 y se aumentaron 20 pesos, valor por por Kg: " << value << endl;
		}
		else if (tamanio == 2)
		{
			cout << "Es tipo A tamaño 2 y se aumentaron 30 pesos, valor por por Kg: " << value << endl;
		}
		else
		{
			cout << "Eror digito un tamaño diferente: " << endl;
		}
	}
	else if (tipo == 4)
	{
		if (tamanio == 1)
		{
			cout << "Es tipo B y tamaño 1 y se disminuyen 30 pesos, valor por por Kg: " << value << endl;
		}
		else if (tamanio == 2)
		{
			cout << "Es tipo B tamaño 2 y se disminuyen 50 pesos, valor por por Kg: " << value << endl;
		}
		else
		{
			cout << "Eror digito un tamaño diferente: " << endl;
		}
	}
	else
	{
		cout << "Eror digito un tipo diferente: " << endl;
	}*/


	cin.get();
	return 0;
}

void Mostrar(int* valCuadra, const int* valEntra, int numByte, int suma) {

	for (int cont = 0; cont < numByte; cont++) {
		cout << "Contador: " << cont << " \tAreglo: " << valEntra[cont] << " \tCuadrado: " << valCuadra[cont] << endl;
	}
	cout << endl;
	cout << "Suma " << suma << endl;
}

void Mostrar2(double* valCuadra, const double* valEntra, int numByte) {

	for (int cont = 0; cont < numByte; cont++) {
		cout << "Contador: " << cont << " \tAreglo: " << valEntra[cont] << " \tArea: " << valCuadra[cont] << endl;
	}
}
