//7. En un almacén el valor unitario de cada artículo es de $20.000, por cada 10 artículos que se compren el 
//almacén rebaja $12.000.Haga un algoritmo que imprima el valor que pagará un cliente por la compra de N artículos
//y la rebaja que obtuvo.

#include "pch.h"
#include <iostream>


using namespace std;

	const int valueByProduct = 20000, discountUnit = 12000;
	int cantProduct = 0;
	int restrictDiscount = 10;
	int totalDiscount = 0;
	float totalSale = 0;

extern "C" int calculateDiscount(int cantProduct, int valueByProduct);

	int CalculateDiscount(int cantProduct, int restrictDiscount)
	{
		while (cantProduct >= restrictDiscount)
		{
			totalDiscount += discountUnit;
			restrictDiscount += 10;
		}

		return totalSale = (cantProduct * valueByProduct) - totalDiscount;
	}

int main()
{
	cout << "Type the cant of products that you wish purchase: "; cin >> cantProduct;
	cout << "Number of products purchased to apply discount: " << restrictDiscount << "\n" << endl;

	totalSale = CalculateDiscount(cantProduct, restrictDiscount);

    cout << "Price by product: $ " << valueByProduct << "\n" << endl;
    cout << "total sale without discount: $ " << (cantProduct * valueByProduct) << "\n" << endl;
	cout << "total discount applied: $ " << totalDiscount << "\n" << endl;
    cout << "Total value sold with discount: $" << totalSale << endl ;

	system("PAUSE");

	return 0;
}

