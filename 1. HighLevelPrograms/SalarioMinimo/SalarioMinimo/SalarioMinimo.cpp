//10. Un empleado gana 3.5 salarios mínimos mensuales vigentes y le deducen el 10 % por un
//préstamo y el 3 % de retención en la fuente.Haga un algoritmo que imprima; el nombre
//del empleado, el salario bruto, el valor del descuento por préstamo, valor del descuento
//por retención y el salario neto del trabajador.

#include "pch.h"
#include <iostream>

using namespace std;

const int SMMV = 781242;

int main()
{
    cout << "Employee: Julian Herrera Giraldo" << "\n" << endl; 
	cout << "Brute salary: " << fixed << SMMV*3.5 << "\n" <<  endl;
	cout << "Discount by credit: " << fixed << (SMMV * 3.5)*.1 << "\n" << endl;
	cout << "Discount by retention: " << fixed << (SMMV * 3.5)*.03 << "\n" << endl;
	cout << "Net salary: " << fixed << fixed << (SMMV * 3.5 -(SMMV * 3.5)*.1) - (SMMV * 3.5)*.03 << "\n" << endl;

	system("PAUSE");
	return 0;
}

