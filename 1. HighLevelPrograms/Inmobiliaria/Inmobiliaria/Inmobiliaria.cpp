//14. Una inmobiliaria vende terrenos a $1.000.000 el metro cuadrado.El cliente debe dar
//una inicial del 15 % y el resto lo paga en 12 cuotas a cero interés.Determine el valor total
//a pagar y el valor de cada cuota conociendo el largo y el ancho del terreno.

#include "pch.h"
#include <iostream>

using namespace std;
int widhtField = 0, heightField = 0;
const int valueEachMt = 1000000;

int main()
{
	cout << "Type width of field in mts: " << "\n"; cin >> widhtField;
	cout << "Type height of field in mts: " << "\n"; cin >> heightField;
	cout << "Total size of field is: " << widhtField*heightField << "mts \n" << endl;
	cout << "Total value to purchase: $" << (valueEachMt*(widhtField * heightField)) << "\n" << endl;
	cout << "the initial quota is of 15% : $" << fixed << (valueEachMt*(widhtField * heightField))*.15 << "\n" << endl;
    cout << "12 quota (each one of): $" << fixed << ((valueEachMt*(widhtField * heightField)) - ((valueEachMt*(widhtField * heightField))*.15))/12 << "\n" << endl;

	system("PAUSE");
}
