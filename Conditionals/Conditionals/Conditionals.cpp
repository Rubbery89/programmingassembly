// Conditionals.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include "pch.h"
#include <iostream>
#include <ctime>
#include <string>
#include <array>

using namespace std;

#pragma region Methods Assembly

extern "C" int pago(int area, int metro);
extern "C" int cuota(int valor);
extern "C" int cuotaTotal(int valor, int forma);

extern "C" int adminSueldo(int sueldo);
extern "C" int opeSueldo(int admin);
extern "C" int ejeSueldo(int admin);

extern "C" int calculateDiscount(int cantProduct, int init, int restriction, int discountByUnit);
extern "C" int calculateTotalSale(int cantProduct, int valueByProduct, int totalDiscount);
extern "C" int saleWithDiscount(int totalSale, int totalDiscount);

extern "C" int perfecto(int num);
extern "C" int FunctionC(int num, int* res);
extern "C" int ProcessDownLevel(int SelectedColor, int ValuePurchase);
extern "C" int divisor(int num, int b);

extern "C" int total(int ancho, int largo, int terreno);
extern "C" int CalculateCuota(int valor);
extern "C" int CuotasByMonth(int valor, int inicial);
extern "C" int HasRestriction(int number, int day);
#pragma endregion

#pragma region Global variables

int option = 0;

#pragma endregion

#pragma region Methods High Level


void applyDiscount()
{
	const int valueByProduct = 20, discountUnit = 5;
	int cantProduct = 0;
	int restrictDiscount = 10;
	int init = 0;
	int totalDiscount = 0;
	float totalSale = 0;

	std::cout << "Type the cant of products that you wish purchase: "; std::cin >> cantProduct;
	std::cout << "Price by product: $ " << valueByProduct << " dollars \n" << endl;
	std::cout << "If you purchase more than ten products has right to \n $5 dollars of discount by each additional product\n" << endl;
	totalDiscount = calculateDiscount(cantProduct, init, restrictDiscount, discountUnit);
	std::cout << "total discount applied: $" << totalDiscount << " dollars\n" << endl;

	totalSale = calculateTotalSale(cantProduct, valueByProduct, totalDiscount);
	std::cout << "total sale without discount: $" << totalSale << " dollars\n" << endl;

	std::cout << "Total value sold with discount: $" << saleWithDiscount(totalSale, totalDiscount) << " dollars\n" << endl;

}

void SelectColor()
{
	//Parameters first program
	const double totalValuePurchase = 50000;
	double discount_white = 0, discount_green = 0.1, discount_yellow = 0.25, discount_blue = 0.5, discount_red = 1;

	srand(time(NULL));
	int color = rand() % 5;
	double discount = 0;
	char _color = 'n';

	if (color == 0)
	{
		discount = discount_white;
		std::cout << "You have selected white color, total discount " << discount * 100 << "%\n" << endl;
	}
	else if (color == 1)
	{
		discount = discount_green;
		std::cout << "You have selected green color, total discount " << discount * 100 << "%\n" << endl;
	}
	else if (color == 2)
	{
		discount = discount_yellow;
		std::cout << "You have selected yellow color, total discount " << discount * 100 << "%\n" << endl;
	}
	else if (color == 3)
	{
		discount = discount_blue;
		std::cout << "You have selected blue color, total discount " << discount * 100 << "%\n" << endl;
	}
	else if (color == 4)
	{
		discount = discount_red;
		std::cout << "You have selected red color, total discount " << discount * 100 << "%\n" << endl;
	}

	std::cout << "Value purchase: $" << totalValuePurchase << "\n" << endl;
	std::cout << "Total to pay: $" << totalValuePurchase - (totalValuePurchase*discount) << "\n\n" << endl;

	std::cout << "__________________________________ ASSEMBLY RESULT___________________________________________" << endl;
	
	//Init value purchase
	int _totalValuePurchase = 50;
	int totalResultAssembly = ProcessDownLevel(color, _totalValuePurchase);

	std::cout << "ASSEMBLY ====> Total to pay: $" << totalResultAssembly << "\n\n" << endl;
}

void CalcuateSalary()
{
	int sueldo, admin, eje, ope;
	std::cout << "Sueldo" << endl;
	std::cin >> sueldo;
	admin = adminSueldo(sueldo);
	ope = opeSueldo(admin);
	eje = ejeSueldo(admin);
	std::cout << "Sueldo Admin: " << admin << endl;
	std::cout << "Sueldo Operario: " << ope << endl;
	std::cout << "Sueldo Ejecutivo: " << eje << endl;
}

void CalculatePriceSale()
{
	int area, metro, valor, forma, inicial, inicialTotal;

	std::cout << "Area propiedad: " << endl;
	std::cin >> area;
	std::cout << "Valor metro: " << endl;
	std::cin >> metro;
	std::cout << "Forma de pago: \n1:Contado \n 2:Cheque \n 3:Plazos \n\n" << endl;
	std::cin >> forma;

	valor = pago(area, metro);
	inicial = cuota(valor);
	inicialTotal = cuotaTotal(inicial, forma);
	std::cout << "Valor propiedad: " << valor << endl;
	std::cout << "Cuota inicial: " << inicial << endl;
	std::cout << "Cuota inicial Total: " << inicialTotal << endl;
}

void findPerfectNumber()
{
	int num = 6, p = 4, i = 0;
	while (i < p) {

		num = perfecto(num);
		std::cout << num << endl;
		num += 1;
		i += 1;
	}
}

void CalculateNextNumber()
{
	int num;
	int residuo = 0, res;
	std::cout << "Ingrese un numero entero: \t";
	std::cin >> num;

	int siguiente = FunctionC(num, &res);
	if (siguiente == 0)
	{
		std::cout << "Error, porque el numero ingresado no debe ser: " << siguiente << endl;
	}
	else if (siguiente > 0)
	{
		std::cout << "El siguiente numero par es: \t" << siguiente << endl;
	}
	else
	{
		std::cout << "El valor absoluto es: \t" << siguiente << endl;
	}
}

void RulesDriving()
{
	int dayOfWeek = 0, restrictSelection = 0;
	int daysOfWeek[5] = { 1, 2, 3, 4, 5 };
	char registerCar[6] = "";
    bool hasRestriction;

	//restriction in a Week
	int monday[4] = { 0,1,2,3 };
	int tuesday[4] = { 4,5,6,7 };
	int wednesday[4] = { 8,9,0,1 };
	int thursday[4] = { 2,3,4,5 };
	int friday[4] = { 6,7,8,9 };

	while (restrictSelection == 0)
	{
		std::cout << "Select a day of week to validate if you have (Pico y placa) today :-( :\n 1. Monday \n 2. Tuesday \n 3. Wednesday \n 4. Thursday \n 5. Friday \n\n"; std::cin >> dayOfWeek;
		int *validateSelection = find(begin(daysOfWeek), end(daysOfWeek), dayOfWeek);
		
		if (validateSelection != std::end(daysOfWeek))
	    {
			restrictSelection = 1;
			cerr << "" << endl;
		}					
		else
			cerr << "You selection is not valid, try again ==>" << std::distance(daysOfWeek, validateSelection) << endl;
	}

	std::cout << "Please type your car registration number:\t"; std::cin >> registerCar;
	
	if (strlen(registerCar) == 5  || strlen(registerCar) == 6 )
	{		
		size_t length = strlen(registerCar);
		char number = registerCar[length-1];
		
		switch (dayOfWeek)
		{
			case 1:			
				hasRestriction = (number == '0' || number == '1' || number == '2' || number == '3');
                break;
			case 2:
				hasRestriction = (number == '4' || number == '5' || number == '6' || number == '7');
				break;
			case 3:
				hasRestriction = (number == '8' || number == '9' || number == '0' || number == '1');
				break;
			case 4:
				hasRestriction = (number == '2' || number == '3' || number == '4' || number == '5');
				break;
			case 5:
				hasRestriction = (number == '6' || number == '7' || number == '8' || number == '9');
				break;
			default:
				std::cout << "Something leave wrong :-(.." << endl;
				break;
		}
			if(hasRestriction)
				std::cout << "you cannot leave on your car, because has restriction this day" << endl;
			else
				std::cout << "you don't have any restriction today :-)\n\n" << endl;

			std::cout << "_________________________ASSEMBLY__________________________________" << endl;
			int numberAsm = -1;
			
				if (number == '0')
					numberAsm = 0;
				else if (number == '1')
					numberAsm = 1;
				else if (number == '2')
					numberAsm = 2;
				else if (number == '3')
					numberAsm = 3;
				
						
				if (number == '4')
					numberAsm = 4;
				else if (number == '5')
					numberAsm = 5;
				else if (number == '6')
					numberAsm = 6;
				else if (number == '7')
					numberAsm = 7;
				
						
				if (number == '8')
					numberAsm = 8;
				else if (number == '9')
					numberAsm = 9;
				else if (number == '0')
					numberAsm = 0;
				else if (number == '1')
					numberAsm = 1;

				if (number == '2')
					numberAsm = 2;
				else if (number == '3')
					numberAsm = 3;
				else if (number == '4')
					numberAsm = 4;
				else if (number == '5')
					numberAsm = 5;				
				
				if (number == '6')
					numberAsm = 6;
				else if (number == '7')
					numberAsm = 7;
				else if (number == '8')
					numberAsm = 8;
				else if (number == '9')
					numberAsm = 9;				
			
			   int response = HasRestriction(numberAsm, dayOfWeek);


			   if (response == 1)
				   std::cout << "\nyou cannot leave on your car, because has restriction this day" << endl;
			   else
				   std::cout << "\nyou don't have any restriction today :-)\n\n" << endl;
	}
	else
	{
		std::cout << "Your car registration number is not valid" << endl;
	}
	
}

void Divisor()
{
	int num, i = 6, a, b = 27;

	std::cout << "Ingrese numero: " << endl;
	cin >> num;
	
	if (num < 50) {
		std::cout << "Numero invalido :-( \n\n" << endl;
	}
	else 
	{
		while (i != 1) {

			a = divisor(num, b);
			std::cout << a << endl;
			b = a - 1;
			if (a == 1) {
				i = 1;
			}
		}
	}

}

void Inmobiliaria()
{
	int widhtField = 0, heightField = 0;
	const int valueEachMt = 1000000;

	std::cout << "Type width of field in mts: " << "\n"; cin >> widhtField;
	std::cout << "Type height of field in mts: " << "\n"; cin >> heightField;
	std::cout << "Total size of field is: " << widhtField * heightField << "mts \n" << endl;
	std::cout << "Total value to purchase: $" << (valueEachMt*(widhtField * heightField)) << "\n" << endl;
	std::cout << "the initial quota is of 15% : $" << fixed << (valueEachMt*(widhtField * heightField))*.15 << "\n" << endl;
	std::cout << 
	"12 quota (each one of): $" << 
	fixed << ((valueEachMt*(widhtField * heightField)) - ((valueEachMt*(widhtField * heightField))*.15)) / 12 << "\n" << endl;


	std::cout << "_______________________________________________ASSEMBLY___________________________________________________________" << "\n\n" << endl;

	int ancho, largo, terreno = 100, valor, inicial, pagos;
	std::cout << "Ingrese ancho" << endl;
	cin >> ancho;
	std::cout << "Ingrese largo" << endl;
	cin >> largo;
	valor = total(ancho, largo, terreno);
    std::cout << "Total terreno :" << valor << endl;
	inicial = CalculateCuota(valor);
	std::cout << "Cuota inicial :" << inicial << endl;
	pagos = CuotasByMonth(valor, inicial);
	std::cout << "Cuotas mensual :" << pagos << endl;
}

#pragma endregion

int main()
{
	bool value = true;
	char control = 'n';
	int ctrlMessage = 0;

	while (value)
	{
		if (ctrlMessage == 0)
		{
			std::cout << 
				"CONDITIONALS\n\n"
				"Select an option to solve: \n 1. Un almacen efectua una promocion en la cual se hace un descuento sobre el"
				"valor total de la compra, \n segun el color de una balota que el cliente saque al pagar en la caja. Si la"
				"balota es de color blanco\n no se le hara descuento, si es verde se le dara un 10%, si es amarilla un 25%,"
				"si es azul un 50%\n y si es roja el 100%. Haga un algoritmo que imprima el valor a pagar por el cliente y"
				"el valor del descuento obtenido. \n\n\n 2. Realizar un algoritmo que lea el dia de la semana (lunes, martes,…)"
				"y el ultimo digito de la placa de un vehículo\n e imprimir un mensaje informando si esta en pico y placa o no,"
				"segun la siguiente informacion.	\n Lunes: 0, 1, 2, 3 \n Martes : 4, 5, 6, 7 \n Miercoles : 8, 9, 0, 1 \n"
				"Jueves : 2, 3, 4, 5 \n Viernes : 6, 7, 8, 9 \nNota : leer toda la placa como un String \n\n\n 3. Haga un "
				"algoritmo que lea un numero entero, si el numero es mayor a cero imprimir el siguiente numero par\n del numero"
				"ingresado, si es menor a cero imprimir el valor absoluto del numero,\n si es cero imprimir dato incorrecto. \n\n "
				"4. Se tienen el area de una propiedad, el valor del metro cuadrado y la forma de pago de la cuota inicial.\n"
				"Haga un algoritmo que imprima el precio de venta y el valor a pagar de la cuota inicial \n(45% del valor de"
				" la propiedad), teniendo en cuenta que por pago de contado se da un descuento del 10%\n  sobre la cuota inicial,"
				"por pago con cheques el 6% y si paga a plazos se le recarga un 8% sobre la cuota inicial.\n\n"
				
				"FIRST WORKSHOP\n\n"
				"5. Suponga que en una empresa hay tres tipos de empleados: ejecutivos, administrativos"
				"y operarios.\n La empresa tiene un salario basico de X dolares mensuales.El ejecutivo"
				"gana el doble del administrativo, \n el operario gana dos terceras partes del administrativo"
				"mas un 30 % y el administrativo gana un salario\n basico mas un 20 % .Haga un algoritmo"
				"que imprima el salario en pesos de cada uno de los tipos de empleados.\n\n"
				
				"6. En un almacen el valor unitario de cada articulo es de $20, por mas de 10 articulos que se compren el \n"
				"almacen rebaja $5 por cada producto adicional.Haga un algoritmo que imprima el valor que pagara un cliente \n"
				"por la compra de N articulos y la rebaja que obtuvo."
				
				"\n\nLOOPS\n\n"
				"7. Haga un algoritmo que imprima los primeros 20 numeros perfectos mayores a 10. Nota: un numero perfecto es aquel\n"
				"donde la suma de sus divisores (excluyendo el mismo numero) es igual al numero. Ejemplo: los divisores de 6 son \n"
				"1, 2 y 3 (1+2+3) = 6, por lo tanto 6 es un numero perfecto.\n\n"
				"8. Haga un algoritmo que permita ingresar un numero entero mayor a 50 e imprima los divisores que tiene"
				"dicho\n numero exceptuando la unidad y el mismo numero.\n\n"
				
				"9. Una inmobiliaria vende terrenos a $1.000.000 el metro cuadrado. El cliente debe dar una inicial del 15% "
				"y el resto lo paga en 12 cuotas a cero interés. Determine el valor total a pagar y el valor de cada cuota "
				"conociendo el largo y el ancho del terreno.\n\n"; std::cin >> option;
		}
		else
		{
			std::cout << "_______________________________________________________________________________________________________\n\n" << endl;
			std::cout << "\nSelect an option again:\n\n"; std::cin >> option;
		}

		ctrlMessage = 1;

		switch (option)
		{
		case 1:
			SelectColor();
			break;
		case 2:
			RulesDriving();
			break;
		case 3:
			CalculateNextNumber();
			break;
		case 4:
			CalculatePriceSale();
			break;
		case 5:
			CalcuateSalary();
			break;
		case 6:
			applyDiscount();
			break;
		case 7:
			findPerfectNumber();
			break;
		case 8:
			Divisor();
			break;
		case 9:
			Inmobiliaria();
			break;
		default:
	std::	cout << " _     ___    _  "<<"\n"
				    "| |   / _ \\  | | "<<"\n"
					"| |  | | | | | | "<<"\n"
				 	"| |__| |_| | | |___ "<<"\n"
					"|____|\\___/  |_____| "<<"\n\n" << endl;
			std::system("PAUSE");
			main();			
		}
		std::cout << "try again? Y/N:  ";
		std::cin >> control;

		value = (control == 'Y' || control == 'y');
	
	}

	return 0;
}


