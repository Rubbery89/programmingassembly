.386
.model flat, c
.data
  ; vbles problem color balls to apply discount
  ballWhite DD 0
  ballGreen DD 1
  ballYellow DD 2
  ballBlue DD 3
  ballRed DD 4
  DisWhite DD 0
  DisGreen DD 1
  DisYellow DD 2
  DisBlue DD 5
  DisRed DD 3

  initValue DD 1

.code
; 1 Conditionals
ProcessDownLevel PROC
  push ebp
  mov ebp, esp
  mov eax, [ebp+8] ; color param
  mov ebx, [ebp+12] ; purchase

  cmp eax, ballWhite
  jz white

  cmp eax, ballGreen
  je green

  cmp eax, ballYellow
  je yellow

  cmp eax, ballBlue
  je blue

  cmp eax, ballRed
  je red

  white:
    mov edx, DisWhite
	imul edx, ebx
	jmp finallyProcess
  
  green:
    mov edx, DisGreen
	imul edx, ebx
	jmp finallyProcess

  yellow:
    mov edx, DisYellow
	imul edx, ebx	
	jmp finallyProcess

  blue:
    mov edx, DisBlue
	imul edx, ebx
	jmp finallyProcess

  red:
    mov edx, DisRed
	imul edx, ebx
	jmp finallyProcess

	finallyProcess:
	 mov eax, edx
	 mov ecx, ebx	 
	 sub eax, ecx
	 pop ebp
	 ret
ProcessDownLevel ENDP


;11 conditional
 pago proc
  push ebp
  mov ebp,esp
  mov eax,[ebp+8]
  mov ebx,[ebp+12]
  mul ebx

  pop ebp
  ret
 pago endp

 cuota proc
  push ebp
  mov ebp,esp
  mov eax,[ebp+8]
  mov ebx,45
  mov ecx,100
  mul ebx
  cdq
  div ecx

  pop ebp
  ret
 cuota endp

 cuotaTotal proc
  push ebp
  mov ebp,esp
  mov eax,[ebp+8]
  mov ebx,[ebp+12]
  mov ecx,1

  cmp ebx,ecx
  je contado

  mov ecx,2
  cmp ebx,ecx
  je cheque

  mov ecx,3
  cmp ebx,ecx
  je plazo

  contado:
    mov ebx,10
	mov ecx,100
	mul ebx
	cdq
	div ecx
	mov ebx,[ebp+8]
	sub ebx,eax
	mov eax,ebx
	jmp salida

  cheque:
    mov ebx,6
	mov ecx,100
	mul ebx
	cdq
	div ecx
	mov ebx,[ebp+8]
	sub ebx,eax
	mov eax,ebx
	jmp salida

   plazo:
    mov ebx,8
	mov ecx,100
	mul ebx
	cdq
	div ecx
	mov ebx,[ebp+8]
	add ebx,eax
	mov eax,ebx
	jmp salida


  salida:
   pop ebp
   ret
 cuotaTotal endp

 ;7 first workshop

 calculateDiscount PROC
   
   PUSH EBP
   MOV EBP, ESP
   MOV EAX, [EBP+8]
   MOV EBX, [EBP+12]
   MOV ECX, [EBP+16]
   MOV EDX, [EBP+20]

HasDiscount:
   CMP ECX, EAX
   JGE leaveProgram
   ADD EBX, EDX
   INC ECX
   JMP HasDiscount

leaveProgram:
     POP EBP
	 MOV EAX, EBX

	 RET
calculateDiscount ENDP
  
calculateTotalSale PROC
   
   PUSH EBP
   
   MOV EBP, ESP
   MOV EAX, [EBP+8]
   MOV EBX, [EBP+12]
   ;MOV EDX, [EBP+16] 
   MUL EBX 
   ;SUB EBX, EDX
   POP EBP
   
   RET

calculateTotalSale ENDP

saleWithDiscount PROC
   PUSH EBP
   MOV EBP, ESP
   MOV EAX, [EBP+8]
   MOV EBX, [EBP+12]
   SUB EAX, EBX
   POP EBP
   RET;

saleWithDiscount ENDP

 ;11 first workshop
 
 adminSueldo proc
   push ebp
   mov ebp,esp
   mov eax,[ebp+8]
   mov ebx,20
   mov ecx,100

   mul ebx
   cdq
   div ecx
   add eax,[ebp+8]
   pop ebp
   ret
  adminSueldo endp


  opeSueldo proc
   push ebp
   mov ebp,esp
   mov eax,[ebp+8]
   mov ebx,2
   mul ebx
   mov ecx,3
   cdq
   div ecx
   mov [ebp+12],eax
   mov ebx,30
   mul ebx
   mov ecx,100
   cdq
   div ecx
   add eax,[ebp+12]

   pop ebp
   ret
  opeSueldo endp

  ejeSueldo proc
   push ebp
   mov ebp,esp
   mov eax,[ebp+8]
   mov ebx,2
   mul ebx
   pop ebp
   ret
  ejeSueldo endp

  ;11 Loops
  perfecto proc
  push ebp
  mov ebp,esp

  mov eax,[ebp+8]
  mov ebx,0
  mov [ebp+12],ebx
  mov ecx,1

  para:
   cmp eax,ecx
   jbe comparacion
   mov edx,0
   div ecx
   mov eax,[ebp+8]
   cmp ebx,edx
   je aumento
   inc ecx
   jmp para

   aumento:
     add [ebp+12],ecx
	 inc ecx
	 jmp para
   
   comparacion:
     cmp eax,[ebp+12]
	 je salida
	 add eax,1
	 mov [ebp+8],eax
	 mov ebx,0
	 mov [ebp+12],ebx
	 mov ecx,1
	 jmp para

   salida:
    pop ebp
    ret  

 perfecto endp

 ;14 Conditionals
 FunctionC proc
		mov eax, [esp+4]
		mov edx, [esp+8]
		mov ebx, 2

		cmp eax, 0
		je error
		
		cmp eax, 0
		jle numnegativo

		cdq
		idiv ebx
		cmp edx, 0
		je par
		jmp numimpar

		par:
			imul eax, 2
			add eax,2
			jmp fin

		numimpar:
			imul eax, 2
			add eax,2
			jmp fin

		error:
			jmp fin

		numnegativo:
			mov ecx, eax
			imul ecx, 2
			sub eax, ecx
		fin:
		ret
FunctionC endp

; 14 LOOPS
divisor proc
   push ebp
   mov ebp,esp

   mov eax,[ebp+8]
   mov ebx,[ebp+12]
   mov ecx,0
  ;mov edx,0
   
  ;div ebx

  ;cmp edx,ecx
  ;je salida

   para:     
	 mov eax,[ebp+8]	
	 cdq
	 div ebx
	 dec ebx
	 cmp edx,ecx
	 ja para
	 inc ebx
	 mov eax,ebx	 
	 jmp salida	 
    
	salida:
	  pop ebp
	  ret

 divisor endp

 ;14 first homeWork
total proc
  push ebp
  mov ebp,esp
  mov eax,[ebp+8]
  mov ebx,[ebp+12]
  mov ecx,[ebp+16]
  mul ebx
  mul ecx
  pop ebp
  ret
total endp

CalculateCuota proc
  push ebp
  mov ebp,esp
  mov eax,[ebp+8]
  mov ebx,15
  mov ecx,100

  mul ebx
  cdq
  div ecx

  pop ebp
  ret
CalculateCuota endp

CuotasByMonth proc
  push ebp
  mov ebp,esp

  mov eax,[ebp+8]
  mov ebx,[ebp+12]
  mov ecx,12

  sub eax,ebx  
  cdq
  div ecx

  pop ebp
  ret
CuotasByMonth endp

HasRestriction proc
  push ebp
  mov ebp,esp

  mov eax,[ebp+8]  ;catch register number
  mov ebx,[ebp+12] ; catch the day
     
   cmp ebx, 1
   je Lunes
   cmp ebx, 2
   je Martes
   cmp ebx, 3
   je Miercoles
   cmp ebx, 4
   je Jueves
   cmp ebx, 5
   je Viernes
   

   Lunes:
     cmp eax, 0
	 je TrueRestriction
	 cmp eax, 1
	 je TrueRestriction
	 cmp eax, 2
	 je TrueRestriction
	 cmp eax, 3
	 je TrueRestriction
	 mov eax, 0
	 jmp FinallyFn

   Martes:
     cmp eax, 4
	 je TrueRestriction
	 cmp eax, 5
	 je TrueRestriction
	 cmp eax, 6
	 je TrueRestriction
	 cmp eax, 7
	 je TrueRestriction
	 mov eax, 0
	 jmp FinallyFn

   Miercoles:
     cmp eax, 8
	 je TrueRestriction
	 cmp eax, 9
	 je TrueRestriction
	 cmp eax, 0
	 je TrueRestriction
	 cmp eax, 1
	 je TrueRestriction
	 mov eax, 0
	 jmp FinallyFn

   Jueves:
     cmp eax, 2
	 je TrueRestriction
	 cmp eax, 3
	 je TrueRestriction
	 cmp eax, 4
	 je TrueRestriction
	 cmp eax, 5
	 je TrueRestriction
	 mov eax, 0
	 jmp FinallyFn

   Viernes:
     cmp eax, 6
	 je TrueRestriction
	 cmp eax, 7
	 je TrueRestriction
	 cmp eax, 8
	 je TrueRestriction
	 cmp eax, 9
	 je TrueRestriction
	 mov eax, 0
	 jmp FinallyFn

   TrueRestriction:
     mov eax , 1
	 jmp FinallyFn

   FinallyFn:
    pop ebp
	ret

HasRestriction endp
end